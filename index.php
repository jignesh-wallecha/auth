<?php
require_once ("app/init.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Index</title>
</head>
<body>
       <?php if($auth->check()) : ?>
            <h3>Welcome, <?= $auth->user()->username ?></h3>
            <p>You are signed in <a href="signout.php">Sign Out</a></p>     
       <?php else: ?>
       		<p>You are not signed in: <a href="signin.php">Sign In</a> OR <a href="signup.php">REGISTER</a></p>
       <?php endif;?>
</body>
</html>