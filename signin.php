<?php
require "app/init.php";
if(!empty($_POST)) 
{
       $username = $_POST['username'];
       $password = $_POST['password'];
       $remember_me = $_POST['rem'];
       $status = $auth->signin($username, $password);
       if($status) 
       {
              if($remember_me) 
              {
                     $user = $userHelper->findUserByUsername($username);
                     $token = $tokenHandler->createRememberMeToken($user->id);

                     setcookie("token", $token['token'], time()+1800);
              }
              header('Location: index.php');
       }
       else 
       {
              echo "Wrong Username/Password!";
       }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Sign In</title>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<?php if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'], 1)): ?>
       <h3> You are already signed in </h3>
<?php else: ?> 
       <h1>Login</h1>
       <form action="signin.php" method="POST">
              <fieldset>    
                      <p><a href="forgotpassword.php">Forgot Password</a></p>
                     <legend>Sign In</legend>
                     <label>
                            Username:
                            <input type="text" name="username">
                     </label>
                     <label>
                            Password:
                            <input type="password" name="password">
                     </label>
                     <br><br>
                     <label>
                            <input type="checkbox" name="rem" checked="on"> Remember Me
                     </label>
                     <br><br>
                     <input type="submit" name = "submit" value="Sign in">
              </fieldset>
       </form>
<?php endif; ?>
</body>
</html>
