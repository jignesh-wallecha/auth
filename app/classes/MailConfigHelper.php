<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once('vendor/autoload.php');

class MailConfigHelper 
{
	public static function getMailer(int 
		$debugMode=0): PHPMailer 
	{
		$mail = new PHPMailer();

		$mail->isSMTP();
		$mail->Host = 'smtp.mailtrap.io';
		$mail->port = 2525;
		$mail->SMTPAuth = true;
		$mail->Username = '2148c36d9748f2';
		$mail->Password = '7cedf0af978ea3';
		$mail->SMTPSecure = 'tls';
		$mail->isHtml(true);
		$mail->setFrom('admin@jigneshwallecha.com', 'Admin<SL Team>');

		return $mail;

	}
}