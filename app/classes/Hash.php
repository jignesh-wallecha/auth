<?php

class Hash
{
	public static function make($plaintext): string 
	{
		return password_hash($plaintext, PASSWORD_BCRYPT, ['cost' => 10]);
	}

	public static function verify($plaintext, $hashed): bool
	{	
		return password_verify($plaintext, $hashed);
	}

	public static function generateToken($id) 
	{
		return hash('sha256', $id . round(microtime(true)*1000) . strrev($id) . rand());
	}
}