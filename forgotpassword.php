<?php
require 'app/init.php';

if(!empty($_POST)) 
{
       $email = $_POST['email'];
       $user = $userHelper->findUserByEmail($email);
       if($user) 
       {
              $tokenData = $tokenHandler->createForgotPasswordToken($user->id);
              if($tokenData) 
              {
                     //var_dump($mail);
                     $mail->addAddress($user->email);
                     $mail->Subject = "Password Recovery...";
                     $mail->Body = "Use this link within 10 minutes to reset the password: <br>";
                     $mail->Body .= "<a href='http://localhost:9000/resetpassword.php?t={$tokenData['token']}'>Reset Password</a>";

                     if($mail->send()) 
                     {
                            echo "Please check ur email to reset your password!";
                     }
                     else 
                     {
                            echo "Problem sending mail, please try again later!";
                     }
              }
              else 
              {
                     echo "<h3>There is some issue in server, please try again later!</h3>";
              }
       } 
       else
       {
              echo "No such email id found!";
       }
}
else if($auth->check()) 
{
       echo "<h3>You are already signed in, how can u forgot the password!<h3>";
}
else 
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Forgot Password</title>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
       <h1>Forgot Password</h1>
       <form action="forgotpassword.php" method="POST">
              <fieldset>    
                     <legend>Forgot Password</legend>
                     <label>
                            Email:
                            <input type="text" name="email">
                     </label>
                     <input type="submit" value="Reset Password" name = "submit">
              </fieldset>
       </form>
</body>
</html>

<?php

}

