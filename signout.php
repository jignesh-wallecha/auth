<?php
require_once "app/init.php";

if($auth->check()) 
{
	$user = $auth->user();
	//var_dump($user->username);
	$auth->signout();
	// delete the token from database
	$tokenHandler->deleteToken($user->id, 1);

	//clear the token cookie from the browser history
	unset($_COOKIE['token']);
	setcookie('token', '', time()-3600);
	//die("Hello World");

	header("Location: index.php");
}
else 
{
	echo "unautorized access....";
}
