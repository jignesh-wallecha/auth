<?php

require_once "app/init.php";

if(!empty($_POST)) 
{
	$validator->check($_POST, [
		'email' => [
			'required' => true,
			'maxlength' => 200,
			'email' => true,
      'unique' => 'users'
		],
		'username' => [
			'required' => true,
			'maxlength' => 20,
			'minlength' => 3,
      'unique' => 'users'
		],
		'password' => [
			'required' => true,
			'maxlength' => 255,
			'minlength' => 8
		]
	]);

	if($validator->fails()) {
		print_r($validator->errors()->all());
  } else {
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $created = $auth->create([
      'email' => $email,
      'username' => $username,
      'password' => $password 
    ]);

    if($created) {
        echo "reaching to securedpage..." . "<br>";
        header("Location: index.php");
    } else {
      echo "There was some issue while creating your user!";
    }
  }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>Sign Up</title>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
       <form action="signup.php" method="POST">
              <fieldset>    
                     <legend>Sign Up</legend>
                     <label for="">
                            Email
                            <input type="text" name="email">
                           	<?php
                           		if($validator->fails() && $validator->errors()->hasErrors()) {
                           			echo $validator->errors()->first('email');
                           		}
                           	?>

                     </label>
                     <br>
                     <br>
                     <label for="">
                            Username
                            <input type="text" name="username">
                            <?php
                              if($validator->fails() && $validator->errors()->hasErrors()) {
                                echo $validator->errors()->first('username');
                              }
                            ?>
                     </label>
                     <br>
                     <br>
                     <label for="">
                            Password
                            <input type="password" name="password">
                            <?php
                              if($validator->fails() && $validator->errors()->hasErrors()) {
                                echo $validator->errors()->first('password');
                              }
                            ?>
                     </label>
                     <br>
                     <br>
                     <input type="submit" value="Sign Up">
              </fieldset>
       </form>
</body>
</html>