<?php
require 'app/init.php';
if(!empty($_GET['t']))
{
	$token = $_GET['t'];
	if($token && $tokenHandler->isValid($token, 0)):
?>
<form action="resetpassword.php" method = "POST">
	<fieldset>
		<legend>Reset Password</legend>
		<label>
			New Password:
			<input type="password" name="password">
		</label>
		<br><br>
		<input type="hidden" name="t" value="<?= $token; ?>">
		<input type="submit" name="submit" value="Reset Password">
	</fieldset>
</form>
<?php
	else:
		echo "<h3>Invalid link, please try with authenticated link!</h3>";
	endif;
}

//When i submit the form
else if(!empty($_POST))
{
	$password = $_POST['password'];
	$token = $_POST['t'];
	if($tokenHandler->isValid($token, 0)) 
	{
		$user = $userHelper->findUserByToken($token);
		var_dump($user->username);
		if($auth->updatePassword($user->id, $password)) 
		{
			$tokenHandler->deleteTokenByToken($token);
			echo "<h3>Password Reset Successfully! </h3>";
			echo "<br><a href='signin.php'>Sign In</a>";
		}
		else
		{
			echo "Problem with server while updating password, please try again later!";
		}
	}
	else
	{
		echo "Your timeout, please try to generate a new link!";
	}
}
else
{
	echo "<h3> This looks like some fishy activity, we'll report it to admin</h3>";
}